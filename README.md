Overview
========

dicom-xnat is the parent project for bridging between the DICOM and
XNAT data models.

Project | Description
----- | -----
dicom-xnat-mx | translates metadata from a DICOM study into an XNAT experiment
dicom-xnat-sop | defines the relationship between DICOM SOPs and the XNAT data model
dicom-xnat-util | contains utility functions for DICOM data

Instructions
============

The JUnit tests use the NRG
[sample1](http://nrg.wustl.edu/projects/DICOM/sample1.zip)
dataset. Download and unpack the data, then gzip the individual files:

    $ cd /path/to/data
	$ wget http://nrg.wustl.edu/projects/DICOM/sample1.zip
    $ unzip ~/Downloads/sample1.zip
    $ gzip --best sample1/*.dcm

Specify the data directory at build time:

    $ mvn clean package -DargLine=-Dsample.data.dir=/path/to/data/sample1
