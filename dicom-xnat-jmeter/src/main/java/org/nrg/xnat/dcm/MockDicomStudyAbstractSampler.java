/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.samplers.Sampler;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.nrg.dcm.DicomUtils;
import org.nrg.net.SimpleAuthenticator;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie (karchie@wustl.edu)
 *
 */
public abstract class MockDicomStudyAbstractSampler
extends AbstractSampler implements Sampler {
    private static final long serialVersionUID = 1L;
    private static final UIDGenerator uidGenerator = new UIDGenerator();
    
    public static List<String> readLines(final InputStream in) throws IOException {
        final List<String> lines = Lists.newArrayList();
        IOException ioexception = null;
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            String line;
            while (null != (line = reader.readLine())) {
                lines.add(line);
            }
            return lines;
        } catch (IOException e) {
            throw ioexception = e;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                throw ioexception = null == ioexception ? e : ioexception;
            }
        }    
    }
    
    protected final String className = getClass().getSimpleName();
    private final String propBaseURL = className + ".baseURL";
    private final String propActionURI = className + ".importURI";
    private final String propUser = className + ".user";
    private final String propPassword = className + ".password";
    private final String propTemplateFile = className + ".templateFile";
    private final String propDataCounts = className + ".dataCounts";

    private final String propDoesCommit = className + ".doesCommit";
    

    protected MockDicomStudyAbstractSampler(final String name) {
        setName(name);
    }

    public String getBaseURL() {
        return getPropertyAsString(propBaseURL);
    }

    public String getCounts() {
        return getPropertyAsString(propDataCounts);
    }

    public int[] getCountsArray() {
        final String s = getCounts();
        if (Strings.isNullOrEmpty(s)) {
            return new int[0];
        }
        final String[] ss = s.split(",");
        final int[] counts = new int[ss.length];
        for (int i = 0; i < ss.length; i++) {
            counts[i] = Integer.parseInt(ss[i]);
        }
        return counts;
    }

    public boolean getDoesCommit() {
        return getPropertyAsBoolean(propDoesCommit);
    }

    protected String getNewUID() {
        return uidGenerator.call();
    }

    public String getPassword() {
        return getPropertyAsString(propPassword);
    }

    public String getRequestURI() {
        return getPropertyAsString(propActionURI);
    }

    public String getTemplate() {
        return getPropertyAsString(propTemplateFile);
    }

    private File getTemplateFile() {
        return new File(getPropertyAsString(propTemplateFile));
    }

    protected URI getURI() {
        final String base = getPropertyAsString(propBaseURL);
        final String path = getPropertyAsString(propActionURI);
        try {
            return new URI(base + (path.startsWith("/") ? "" : "/") + path);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    protected URL getURL() {
        try {
            return getURI().toURL();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    
    public String getUser() {
        return getPropertyAsString(propUser);
    }

    protected DicomObject makeTemplateObject(final String studyInstanceUID,
            final int seriesNumber, final String seriesInstanceUID,
            final int instanceNumber, final String sopInstanceUID)
    throws IOException {
        final DicomObject o = DicomUtils.read(getTemplateFile(), -1);

        o.putString(Tag.StudyInstanceUID, VR.UI, studyInstanceUID);
        o.putString(Tag.SeriesInstanceUID, VR.UI, seriesInstanceUID);
        o.putInt(Tag.SeriesNumber, VR.IS, seriesNumber);
        o.putString(Tag.SOPInstanceUID, VR.UI, sopInstanceUID);
        o.putInt(Tag.InstanceNumber, VR.IS, instanceNumber);
        
        return o;
    }
    
    protected long getTemplateObjectSize() {
        return getTemplateFile().length();
    }

    protected void setAuthenticator() {
        Authenticator.setDefault(new SimpleAuthenticator(getUser(), getPassword()));
    }

    public String setBaseURL(final String url) {
        final String old = getBaseURL();
        setProperty(propBaseURL, url);
        return old;
    }
  
    public String setCounts(final String counts) {
        final String old = getCounts();
        setProperty(propDataCounts, counts);
        return old;
    }
    
    public boolean setDoesCommit(final boolean doesCommit) {
        final boolean old = getDoesCommit();
        setProperty(propDoesCommit, doesCommit);
        return old;
    }
    
    public String setImportURI(final String uri) {
        final String old = getRequestURI();
        setProperty(propActionURI, uri);
        return old;
    }
    
    public String setPassword(final String p) {
        final String old = getPassword();
        setProperty(propPassword, p);
        return old;
    }
    
    public String setTemplate(final String path) {
        final String old = getTemplate();
        setProperty(propTemplateFile, path);
        return old;
    }
    
    public String setUser(final String u) {
        final String old = getUser();
        setProperty(propUser, u);
        return old;
    }
}
