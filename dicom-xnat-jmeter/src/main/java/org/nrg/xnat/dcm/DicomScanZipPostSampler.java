/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.ByteStreams;

/**
 * @author Kevin A. Archie (karchie@wustl.edu)
 *
 */
public final class DicomScanZipPostSampler extends MockDicomStudyAbstractSampler implements Sampler {
    private static final long serialVersionUID = 1L;

    private class ZipSeriesUploader implements Callable<Set<String>> {
        private static final String CONTENT_TYPE_HEADER = "Content-Type";
        private static final String CONTENT_TYPE_ZIP = "application/zip";

        private final int count;
        private final String studyInstanceUID, seriesInstanceUID;
        private final int seriesNumber;

        public ZipSeriesUploader(final String studyInstanceUID,
                final int seriesNumber, final String seriesInstanceUID,
                final int count) {
            this.studyInstanceUID = studyInstanceUID;
            this.seriesInstanceUID = seriesInstanceUID;
            this.seriesNumber = seriesNumber;
            this.count = count;
        }

        /* (non-Javadoc)
         * @see java.util.concurrent.Callable#call()
         */
        public Set<String> call() throws Exception {
            final boolean useFixedSize = getPropertyAsBoolean(propUseFixedSize);
            return useFixedSize ? sendFixedSize() : sendChunked();
        }

        public Set<String> sendChunked()
        throws IOException {
            final URL url = getURL();
            logger.trace("creating connection to {}", url);
            final HttpURLConnection c = (HttpURLConnection)url.openConnection();
            c.setRequestMethod("POST");
            c.setDoInput(true);
            c.setDoOutput(true);
            c.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
            c.setChunkedStreamingMode(0);
            c.connect();

            try {
                logger.trace("connection open; starting data send");
                IOException ioexception = null;
                final ZipOutputStream zos = new ZipOutputStream(c.getOutputStream());
                try {
                    for (int i = 0; i < count; i++) {
                        final String sopInstanceUID = getNewUID();
                        final DicomObject o = makeTemplateObject(studyInstanceUID, seriesNumber, seriesInstanceUID, i, sopInstanceUID);
                        zos.putNextEntry(new ZipEntry(sopInstanceUID));
                        final DicomOutputStream dos = new DicomOutputStream(new BufferedOutputStream(zos));
                        dos.writeDicomFile(o);
                        dos.flush();
                        zos.closeEntry();
                    }
                } catch (IOException e) {
                    throw ioexception = e;
                } finally {
                    try {
                        zos.close();
                    } catch (IOException e) {
                        throw null == ioexception ? e : ioexception;
                    }
                }
                if (c.getResponseCode() >= 400) {
                    System.err.println(String.format("Request FAILED for %s series %d: %s",
                            studyInstanceUID, seriesNumber, c.getResponseMessage()));
                    for (final String line : readLines(c.getErrorStream())) {
                        System.err.println(line);
                    }
                    return Collections.emptySet();
                } else {
                    return Sets.newLinkedHashSet(readLines(c.getInputStream()));
                }
            } finally {
                c.disconnect();
            }
        }

        public Set<String> sendFixedSize() throws IOException {
            final File tempzip = File.createTempFile("scan-upload", ".zip");
            try {
                logger.trace("creating zip file {}", tempzip);
                IOException ioexception = null;
                final FileOutputStream fos = new FileOutputStream(tempzip);
                try {
                    final ZipOutputStream zos = new ZipOutputStream(fos);
                    try {
                        for (int i = 0; i < count; i++) {
                            final String sopInstanceUID = getNewUID();
                            final DicomObject o = makeTemplateObject(studyInstanceUID, seriesNumber, seriesInstanceUID, i, sopInstanceUID);
                            zos.putNextEntry(new ZipEntry(sopInstanceUID));
                            final DicomOutputStream dos = new DicomOutputStream(new BufferedOutputStream(zos));
                            dos.writeDicomFile(o);
                            dos.flush();
                            zos.closeEntry();
                        }
                    } catch (IOException e) {
                        throw ioexception = e;
                    } finally {
                        try {
                            zos.close();
                        } catch (IOException e) {
                            throw ioexception = null == ioexception ? e : ioexception;
                        }
                    }
                } finally {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
                logger.trace("zip file complete");

                final long zipsize = tempzip.length();

                final URL url = getURL();
                logger.trace("creating connection to {}", url);
                final HttpURLConnection c = (HttpURLConnection)url.openConnection();
                c.setRequestMethod("POST");
                c.setDoOutput(true);
                c.setRequestProperty(CONTENT_TYPE_HEADER, CONTENT_TYPE_ZIP);
                c.setFixedLengthStreamingMode((int)zipsize);
                c.connect();

                try {
                    logger.trace("connection open; starting data send");
                    assert ioexception == null;
                    final FileInputStream fis = new FileInputStream(tempzip);
                    try {

                        final OutputStream os = c.getOutputStream();
                        try {
                            ByteStreams.copy(fis, os);
                        } catch (IOException e) {
                            throw ioexception = e;
                        } finally {
                            try {
                                os.close();
                            } catch (IOException e) {
                                throw ioexception = null == ioexception ? e : ioexception;
                            }
                        }
                    } finally {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            throw ioexception = null == ioexception ? e : ioexception;
                        }
                    }
                    if (c.getResponseCode() >= 400) {
                        System.err.println(String.format("Request FAILED for %s series %d: %s",
                                studyInstanceUID, seriesNumber, c.getResponseMessage()));
                        for (final String line : readLines(c.getErrorStream())) {
                            System.err.println(line);
                        }
                        return Collections.emptySet();
                    } else {
                        return Sets.newLinkedHashSet(readLines(c.getInputStream()));
                    }
                } finally {
                    c.disconnect();
                }
            } finally {
                tempzip.delete();
            }
        }
    }

    public static final String SAMPLER_TITLE = "HTTP POST DICOM scan zip to XNAT";
    private final Logger logger = LoggerFactory.getLogger(DicomScanZipPostSampler.class);

    private final String propUseFixedSize = className + ".useFixedSize";
    private final String propNumThreads = className + ".numThreads";

    public DicomScanZipPostSampler() {
        super(SAMPLER_TITLE);
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.samplers.Sampler#sample(org.apache.jmeter.samplers.Entry)
     */
    public SampleResult sample(final Entry entry) {
        final int[] counts = getCountsArray();
        final SampleResult result = new SampleResult();
        result.setSampleLabel(SAMPLER_TITLE);
        result.setDataType(SampleResult.TEXT);
        result.setSamplerData(Arrays.toString(counts));

        setAuthenticator();
        result.sampleStart();
        try {
            final String studyInstanceUID = getNewUID();
            final ExecutorService executor = Executors.newFixedThreadPool(getNumThreads());
            final CompletionService<Set<String>> completor = new ExecutorCompletionService<Set<String>>(executor);
            final Map<Future<Set<String>>,ZipSeriesUploader> uploaders = Maps.newHashMap();
            for (int series = 0; series < counts.length; series++) {
                final String seriesInstanceUID = getNewUID();
                final ZipSeriesUploader uploader = new ZipSeriesUploader(studyInstanceUID, series, seriesInstanceUID, counts[series]);
                uploaders.put(completor.submit(uploader), uploader);
            }

            final Set<String> uris = Sets.newLinkedHashSet();
            while (!uploaders.isEmpty()) {
                final Future<Set<String>> f = completor.take();
                if (null == uploaders.remove(f)) {
                    System.err.println("warning: unknown task completed: " + f
                            + ", " + uploaders.size() + " remaining");
                }
                uris.addAll(f.get());
            }
            System.out.println("response URIs: " + uris);
            result.setResponseData(Joiner.on(",").join(uris), Charset.defaultCharset().name());
            result.setResponseCodeOK();
            result.setSuccessful(true);
            
            long size = 0;
            int totalCount = 0;
            for (int seriesCount : counts) {
                size += seriesCount * getTemplateObjectSize();
                totalCount += seriesCount;
            }
            result.setBytes((int)size);
            result.setSampleCount(totalCount);

            return result;
        } catch (Throwable t) {
            System.err.println("failed to send DICOM ZIP file");
            t.printStackTrace(System.err);
            logger.info("unable to send DICOM file", t);
            result.setSuccessful(false);
            return result;
        } finally {
            result.sampleEnd();
        }
    }

    public int getNumThreads() {
        return getPropertyAsInt(propNumThreads);
    }

    public int setNumThreads(final int nThreads) {
        final int old = getNumThreads();
        setProperty(propNumThreads, nThreads);
        return old;
    }

    public boolean getUseFixedSize() {
        return getPropertyAsBoolean(propUseFixedSize);
    }

    public boolean setUseFixedSize(final boolean useFixedSize) {
        final boolean old = getUseFixedSize();
        setProperty(propUseFixedSize, useFixedSize);
        return old;
    }
}
