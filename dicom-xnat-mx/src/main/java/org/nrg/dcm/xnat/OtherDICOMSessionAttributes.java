/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.OtherDICOMSessionAttributes
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.UID;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * @author Kevin A. Archie (karchie@wustl.edu)
 *
 */
final class OtherDICOMSessionAttributes {
    private OtherDICOMSessionAttributes() {}    // no instantiation
    static public AttrDefs get() { return s; }

    String foo = UID.SecondaryCaptureImageStorage;
    static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());
}
